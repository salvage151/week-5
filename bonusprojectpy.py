
#Liam "Phicard" Wilson
#9/29/15
#rock paper scissors
import random
roPaSc = True
while roPaSc:
	ranNum = random.randint(1,4)
	chooseWep = int(input('Choose rock: 1, paper: 2, or scissors: 3: '))
	if (ranNum == 1):
		if (chooseWep == 1):
			print("You and the computer chose rock: Tie")
		if (chooseWep == 2):
			print("You chose paper, the computer chose rock: Win")
		if (chooseWep == 3):
			print("You chose scissors, the computer chose rock: Loss")

	if (ranNum == 2):
		if (chooseWep == 1):
			print("You chose rock, the computer chose paper: Loss")
		if (chooseWep == 2):
			print("You and the computer chose paper: Tie")
		if (chooseWep == 3):
			print("You chose scissors, the computer chose paper: Win")

	if (ranNum == 3):
		if (chooseWep == 1):
			print("You chose rock, the computer chose scissors: Win")
		if (chooseWep == 2):
			print("You chose paper, the computer chose scissors: Loss")
		if (chooseWep == 3):
			print("You and the computer chose scissors: Tie")

	keepGoing = input('Try again? Y/N')
	if (keepGoing == 'N' or keepGoing == 'n'):
		roPaSc = False