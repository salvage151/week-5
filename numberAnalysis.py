#Liam Wilson
#10/1/15
#Product 4 Number Analysis

inputList = []
for x in range (0,20):
	userInt = int(input(" Enter 20 Values: "))
	inputList.append(userInt)
def minInput():
	print("Minimum of the values is: ",min(inputList))

def maxInput():
	print("Maximum of the values is: ",max(inputList))

def totalInput():
	print("Total of the values is: ",sum(inputList))

def averageInput():
	average = sum(inputList) / len(inputList)
	print("Average of the list is: ", average)

print(inputList)
minInput()
maxInput()
totalInput()
averageInput()